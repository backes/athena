#include "../FPGATrackSimGNNPatternRecoTool.h"
#include "../FPGATrackSimGNNGraphHitSelectorTool.h"
#include "../FPGATrackSimGNNGraphConstructionTool.h"
#include "../FPGATrackSimGNNEdgeClassifierTool.h"
#include "../FPGATrackSimGNNRoadMakerTool.h"
#include "../FPGATrackSimGNNRootOutputTool.h"

DECLARE_COMPONENT( FPGATrackSimGNNPatternRecoTool )
DECLARE_COMPONENT( FPGATrackSimGNNGraphHitSelectorTool )
DECLARE_COMPONENT( FPGATrackSimGNNGraphConstructionTool )
DECLARE_COMPONENT( FPGATrackSimGNNEdgeClassifierTool )
DECLARE_COMPONENT( FPGATrackSimGNNRoadMakerTool )
DECLARE_COMPONENT( FPGATrackSimGNNRootOutputTool )